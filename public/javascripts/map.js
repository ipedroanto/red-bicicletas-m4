var map = L.map('main_map').setView([-34.6012424,-58.3861497], 13);

    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoiaXBlZHJvYW50byIsImEiOiJja2tkY3NiMm0wOTQ1Mm5vNWJybXF2bG01In0.zNgfnrHHmNZFh67bYJDsmg'
    }).addTo(map);

    // L.marker([-34.60122424,-58.3861497]).addTo(map);
    // L.marker([-34.596932,-58.3808287]).addTo(map);
    // L.marker([-34.599564,-58.3778777]).addTo(map);
    
    $.ajax({ 
        dataType: 'json',
        url: 'api/bicicletas',
        success: function(result){
            console.log(result);
            result.bicicletas.forEach(function(bici){
                 L.marker(bici.ubicacion, {title: bici.id}).addTo(map);
        });
      }

    })