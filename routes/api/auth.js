const express = require('express');
const router = express.Router();
const passport = require ('../../config/passport');

const authApiController = require('../../controllers/api/authApiController');

router.post('/authenticate', authApiController.authenticate);
router.post('/forgot-password', authApiController.forgot_password);
router.post('/facebook_token', passport.authenticate('facebook-token'), authApiController.authFacebookToken);

module.exports = router;
