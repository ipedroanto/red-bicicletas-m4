var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicletaModel');
var Usuario = require('../../models/usuarioModel');
var Reserva = require('../../models/reservaModel');

describe('Testing Usuarios', function() {
  beforeEach(function(done) { // Antes de ejecutar los tests    
    var mongoDB = 'mongodb://localhost/testdb';
    mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB Connection error: '));
    db.once('open', function() {
      console.log('Estamos conectados a la base de datos');
      done();
    });
  });

  describe('Cuando un usuario reserva una bici', () => {
    it('debe existir una reserva', async function(done) {
      
      var usuario = new Usuario({
        nombre: "Pilar",
        email: "Pilar@gmail.com",
        password: "gata",
        confirm_password: "gata" 
      });
      
      usuario = await usuario.save();
      
      var bicicleta = new Bicicleta({code: 1, color: "Verde", modelo: "Urbana", ubicacion: [37.886610834914016, -4.791306293646539]});
      
      bicicleta = await bicicleta.save();

      var hoy = new Date();
      var mañana = new Date();
      
      mañana.setDate(hoy.getDate() + 1);

      usuario.reservar(bicicleta.id, hoy, mañana, function(err, reserva) {
        Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
          expect(reservas.length).toBe(1);
          expect(reservas[0].diasDeReserva()).toBe(2);
          expect(reservas[0].bicicleta.code).toBe(1);
          expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
          done();
        });
      });    
    });
  });

  afterEach(function(done) { // Despues de ejecutar los tests
    Reserva.deleteMany({}, function(err, success) {
      if (err) console.log(err);
      Usuario.deleteMany({}, function(err, success) {
        if (err) console.log(err);
        Bicicleta.deleteMany({}, function(err, success) {
          if (err) console.log(err);
          console.log("mongodb is connected!!, Disconnecting");
          mongoose.disconnect(done); // Despues de realizar las pruebas, cierro la conexión a la db test
        })
      });
    });
  });
});
